import Controller.Commands.ConfigMenuCommand;
import Controller.Commands.FlagRenderCommand;
import Controller.FrameManager;
import Model.Configuration;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        //Ensuring reference to singleton anchor
        Configuration config = Configuration.getInstance();

        FrameManager frameManager = new FrameManager();
        frameManager
                .add(new ConfigMenuCommand())
                .add(new FlagRenderCommand())
                .commit();
    }
}
