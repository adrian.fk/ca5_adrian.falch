package Controller;

import Model.Configuration;
import Utils.ResourceFetcher;
import View.ConfigMenuView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ConfigMenuController implements ControllerInterface, ActionListener {
    private ConfigMenuView view;
    private FrameManager fm;

    public ConfigMenuController(FrameManager fm, ConfigMenuView view) {
        this.view = view;
        this.fm = fm;
    }

    @Override
    public void init() {
        ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");
        ImageIcon favicon;
        try {
            favicon = new ImageIcon(resourceFetcher.getResourceFile().getPath());
            view.setIconImage(favicon.getImage());
        } catch (IOException e) {
            //Ignore Icon
            e.printStackTrace();
        }
        view.setVisible(true);
        view.attachListeners(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(actionEvent.getSource().equals(view.getCreateButton())) {
            processCreateButtonClick();
        }
    }

    private void processCreateButtonClick() {
        if(view.getCbHeight() >= view.getCbWidth()) {
            view.displayErrorMsg();
        }
        else {
            Configuration config = Configuration.getInstance();
            config.setHeight(this.view.getCbHeight());
            config.setWidth(this.view.getCbWidth());
            this.view.setVisible(false);
            fm.finish();
        }
    }
}
