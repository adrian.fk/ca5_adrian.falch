package Controller;

import java.util.ArrayList;

public class FrameManager {
    private ArrayList<CommandInterface> executions;

    public FrameManager() {
        executions = new ArrayList<>();
    }

    public FrameManager(CommandInterface currentExecution) {
        executions = new ArrayList<>();
        executions.add(currentExecution);
    }

    public FrameManager(CommandInterface currentExecution, CommandInterface nextExecution) {
        executions = new ArrayList<>();
        executions.add(currentExecution);
        executions.add(nextExecution);
    }

    public void commit(){
        CommandInterface launchCommand = executions.get(0);
        if(null != launchCommand) {
            executions.remove(0);
            launchCommand.execute(this);
        }
    }

    public FrameManager add(CommandInterface executionCommand) {
        this.executions.add(executionCommand);
        return this;
    }

    public void finish() {
        this.commit();
    }
}
