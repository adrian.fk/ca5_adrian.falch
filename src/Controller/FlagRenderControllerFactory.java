package Controller;

import View.FlagRenderView;

public class FlagRenderControllerFactory implements FlagRenderControllerFactoryI {
    @Override
    public ControllerInterface create(FrameManager fm) {
        return new FlagRenderController(fm, new FlagRenderView());
    }
}
