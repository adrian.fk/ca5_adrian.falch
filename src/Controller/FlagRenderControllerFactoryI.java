package Controller;

public interface FlagRenderControllerFactoryI {
    ControllerInterface create(FrameManager fm);
}
