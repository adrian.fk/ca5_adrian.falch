package Controller;

import View.ConfigMenuView;

public class ConfigMenuControllerFactory implements ConfigMenuControllerFactoryI {
    @Override
    public ConfigMenuController create(FrameManager fm) {
        return new ConfigMenuController(fm, new ConfigMenuView());
    }
}
