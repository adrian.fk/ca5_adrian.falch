package Controller;

import Controller.Threads.CursorCallback;
import Controller.Threads.CursorManager;
import Model.Cell;
import Model.Configuration;
import Utils.Constants;
import Utils.Flags.FlagInterface;
import Utils.Flags.FrenchFlag;
import Utils.ResourceFetcher;
import View.FlagRenderComponents.RenderCell;
import View.FlagRenderView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;

public class FlagRenderController implements ControllerInterface, MouseListener, CursorCallback {
    private Configuration configModel = Configuration.getInstance();
    private FrameManager fm;
    private FlagRenderView view;

    private FlagInterface flag;

    public FlagRenderController(FrameManager fm, FlagRenderView view) {
        this.fm = fm;
        this.view = view;
        this.flag = new FrenchFlag();
    }

    @Override
    public void init() {
        tryFetchFavicon();
        this.view.configure(
                configModel.getWidth(),
                configModel.getHeight(),
                configModel.getWidth() * Constants.cellWidth,
                configModel.getHeight()* Constants.cellHeight,
                this
        );
        this.view.init();

        int numCursors = (configModel.getHeight() * configModel.getWidth()) / (configModel.getHeight() + configModel.getWidth());

        CursorManager cursorManager = new CursorManager(
                numCursors,
                configModel.getWidth(),
                configModel.getHeight(),
                this
        );
        cursorManager.init();
    }

    private void tryFetchFavicon() {
        ResourceFetcher resourceFetcher = new ResourceFetcher("favicon.jpg");
        ImageIcon favicon;
        try {
            favicon = new ImageIcon(resourceFetcher.getResourceFile().getPath());
            this.view.setIconImage(favicon.getImage());
        } catch (IOException e) {
            //Ignore Icon
            e.printStackTrace();
        }
    }


    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        RenderCell renderCell = (RenderCell) mouseEvent.getSource();

        Cell cell = new Cell();
        cell.setPosX(renderCell.getPosX());
        cell.setPosY(renderCell.getPosY());

        this.view.setCell(cell, flag.getColorAt(renderCell.getPosX(), renderCell.getPosY(), configModel.getWidth(), configModel.getHeight()));
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public synchronized void cursorFinished(Cell cell) {
        this.view.setCell(
                cell,
                flag.getColorAt(cell.getPosX(),cell.getPosY(), configModel.getWidth(), configModel.getHeight())
        );
    }
}
