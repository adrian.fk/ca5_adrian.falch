package Controller;

public interface
CommandInterface {
    void execute(FrameManager fm);
}
