package Controller;

public interface ConfigMenuControllerFactoryI {
    ConfigMenuController create(FrameManager fm);
}
