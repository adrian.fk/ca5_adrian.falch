package Controller.Threads;

public interface CursorManagerI {
    void init();
    void stopAll();
}
