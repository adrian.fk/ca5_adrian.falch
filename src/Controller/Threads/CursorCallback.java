package Controller.Threads;

import Model.Cell;

public interface CursorCallback {
    void cursorFinished(Cell cell);
}
