package Controller.Threads;

import java.util.ArrayList;

public class CursorManager implements CursorManagerI{
    private ArrayList<Cursor> cursors;
    CursorCallback callback;
    private int numCursors;
    private int width;
    private int height;


    public CursorManager(int numCursors, int width, int height, CursorCallback callback) {
        this.numCursors = numCursors;
        this.callback = callback;
        this.width = width;
        this.height = height;
        this.cursors = new ArrayList<>();
    }

    public void init() {
        for(int i = 0; i < this.numCursors; i++) {
            Cursor thread = new Cursor(i + 1, this.height, this.width, this.callback);
            this.cursors.add(thread);
            thread.start();
        }
    }

    @Override
    public void stopAll() {

    }
}
