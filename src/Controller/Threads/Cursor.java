package Controller.Threads;

import Model.Cell;

import java.util.Random;

public class Cursor extends Thread {
    private static final int UP = 1;
    private static final int RIGHT = 2;
    private static final int DOWN = 3;
    private static final int LEFT = 4;

    private boolean isRunning;
    private int posX;
    private int posY;
    private int prevDir;

    private CursorCallback callback;
    private int numId;
    private int height;
    private int width;
    private Random random;


    public Cursor(int numId, int height, int width, CursorCallback callback) {
        this.numId = numId;
        this.height = height;
        this.width = width;
        this.callback = callback;
        this.random = new Random();
        this.posX = getRandX();
        this.posY = getRandY();
    }

    @Override
    public void run() {
        super.run();
        isRunning = true;
        while(isRunning) {
            Cell cell = new Cell();
            float sleepTimer = (0.25f * (float) this.numId)*1000;
            long sleepTimerL = (long)sleepTimer;
            try {
                Thread.sleep(sleepTimerL, 0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            moveCursor();


            cell.setPosX(this.posX);
            cell.setPosY(this.posY);
            this.callback.cursorFinished(cell);

        }

    }

    private void moveCursor() {
        int direction = getRandDir();
        if(this.prevDir != 0) direction = this.prevDir;
        while(!validMove(direction)) {
            direction = getRandDir();
        }
        move(direction);
    }

    private void move(int direction) {
        switch(direction) {
            case UP:
                this.posY--;
                break;

            case RIGHT:
                this.posX++;
                break;

            case DOWN:
                this.posY++;
                break;


            default: //LEFT
                this.posX--;
                break;

        }
        this.prevDir = direction;
    }

    private boolean validMove(int direction) {
        switch(direction) {
            case UP:
                return this.posY > 0;

            case RIGHT:
                return this.posX < this.width-1;

            case DOWN:
                return this.posY < this.height-1;


            default: //LEFT
                return this.posX > 0;
        }
    }

    private int getRandDir() {
        return this.random.nextInt(LEFT);
    }

    private int getRandY() {
        return this.random.nextInt(this.height - 1);
    }

    private int getRandX() {
        return this.random.nextInt(this.width - 1);
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }
}
