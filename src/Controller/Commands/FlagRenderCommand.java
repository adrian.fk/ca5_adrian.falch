package Controller.Commands;

import Controller.*;

public class FlagRenderCommand implements CommandInterface {
    @Override
    public void execute(FrameManager fm) {
        FlagRenderControllerFactoryI flagRenderControllerFactory = new FlagRenderControllerFactory();
        ControllerInterface controller = flagRenderControllerFactory.create(fm);
        controller.init();
    }
}
