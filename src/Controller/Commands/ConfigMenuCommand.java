package Controller.Commands;

import Controller.*;

public class ConfigMenuCommand implements CommandInterface {
    @Override
    public void execute(FrameManager fm) {
        ConfigMenuControllerFactoryI configMenuControllerFactory = new ConfigMenuControllerFactory();
        ControllerInterface controller = configMenuControllerFactory.create(fm);
        controller.init();
    }
}
