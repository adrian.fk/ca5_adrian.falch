package Model;

public class Configuration {
    private static Configuration sConfig;

    private int height;
    private int width;

    public static Configuration getInstance() {
        if(sConfig == null) {
            sConfig = new Configuration();
        }
        return sConfig;
    }

    public int getHeight() {
        return sConfig.height;
    }

    public void setHeight(int height) {
        sConfig.height = height;
    }

    public int getWidth() {
        return sConfig.width;
    }

    public void setWidth(int width) {
        sConfig.width = width;
    }
}
