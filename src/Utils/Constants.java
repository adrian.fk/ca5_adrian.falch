package Utils;

import java.awt.*;

public class Constants {
    public static final int cellWidth = 100;
    public static final int cellHeight = 100;

    public interface Colors {
        public static final Color defaultUnpaintedCellColor = Color.DARK_GRAY;
    }
}
