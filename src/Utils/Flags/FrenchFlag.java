package Utils.Flags;

import java.awt.*;

public class FrenchFlag implements FlagInterface {

    @Override
    public Color getColorAt(int x, int y, int width, int height) {
        if(x < width/3) {
            return Color.BLUE;
        }
        else if(x < (width*2)/3) {
            return Color.WHITE;
        }
        else {
            return Color.RED;
        }
    }
}
