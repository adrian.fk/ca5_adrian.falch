package Utils.Flags;

import java.awt.*;

public interface FlagInterface {
    Color getColorAt(int x, int y, int width, int height);
}
