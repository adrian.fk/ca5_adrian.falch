package View.FlagRenderComponents;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class RenderCellArr extends JPanel {
    private ArrayList<RenderCell> cellArray;

    public ArrayList<RenderCell> getCellArray() {
        return cellArray;
    }

    public RenderCellArr(RenderCell initValue, int length, int posX, MouseListener listener) {
        this.cellArray = new ArrayList<>();

        for(int i = 0; i < length; i++) {
            this.cellArray.add(
                    new RenderCell(
                            initValue.getColor(),
                            initValue.getCellX(),
                            initValue.getCellY(),
                            posX,
                            i,
                            listener
                    )
            );
        }




        this.setLayout(new GridLayout(this.cellArray.size(), 1));
        for(RenderCell cell : this.cellArray) {
            this.add(cell);
        }
        this.setVisible(true);
    }

    public void setCellArray(ArrayList<RenderCell> cellArray) {
        this.cellArray = cellArray;
    }

    public void setArrayCell(int index, RenderCell cell) {
        this.cellArray.set(index, cell);
    }

    public void setCellColor(int yIndex, Color newColor) {
        RenderCell cell = this.cellArray.get(yIndex);
        cell.setColor(newColor);
    }

    public int length() { return this.cellArray.size(); }
}
