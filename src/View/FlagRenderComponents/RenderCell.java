package View.FlagRenderComponents;

import Model.Cell;
import Utils.Constants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;

public class RenderCell extends JPanel {
    private Graphics cellGraphics;
    private Cell rootCell;
    private int cellX;
    private int cellY;


    public RenderCell(Color color, int cellX, int cellY, int posX, int posY, MouseListener listener) {
        this.rootCell = new Cell();
        this.rootCell.setColor(color);
        this.cellX = cellX;
        this.cellY = cellY;
        this.rootCell.setPosX(posX);
        this.rootCell.setPosY(posY);

        attachListener(listener);
    }

    private void attachListener(MouseListener listener) {
        this.addMouseListener(listener);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.cellGraphics = g;
        colorRect();
        g.setColor(Color.BLACK);
        g.drawRect(cellX, cellY, Constants.cellWidth, Constants.cellHeight);
    }

    private void colorRect() {
        this.cellGraphics.setColor(this.rootCell.getColor());
        cellGraphics.fillRect(0, 0, Constants.cellWidth - 1, Constants.cellHeight - 1);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(Constants.cellWidth + 2 * cellX, Constants.cellHeight + 2 * cellY);
    }

    public Color getColor() {
        return rootCell.getColor();
    }

    public void setColor(Color color) {
        this.rootCell.setColor(color);
    }

    public int getCellX() {
        return cellX;
    }

    public void setCellX(int cellX) {
        this.cellX = cellX;
    }

    public int getCellY() {
        return cellY;
    }

    public void setCellY(int cellY) {
        this.cellY = cellY;
    }

    public int getPosX() {
        return this.rootCell.getPosX();
    }

    public int getPosY() {
        return this.rootCell.getPosY();
    }
}
