package View;

import Model.Cell;
import Utils.Constants;
import View.FlagRenderComponents.RenderCell;
import View.FlagRenderComponents.RenderCellArr;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class FlagRenderView extends JFrame {
    private boolean isConfigured;
    private ArrayList<RenderCellArr> renderCellArrays;



    public FlagRenderView() throws HeadlessException {
        this.setTitle("Olympic Games 2020");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        this.setLocationRelativeTo(null);
    }

    public void configure(int cellsX, int cellsY, int frameWidth, int frameHeight, MouseListener listener) {
        this.setSize(frameWidth, frameHeight);
        this.renderCellArrays = new ArrayList<>();
        for(int i = 0; i < cellsX; i++) {
            this.renderCellArrays.add(
                    new RenderCellArr(
                            new RenderCell(Constants.Colors.defaultUnpaintedCellColor, 0, 0, 0, 0, listener),
                            cellsY,
                            i,
                            listener
                    )
            );
        }

        this.isConfigured = true;
    }

    public void init() {
        if(this.isConfigured) {
            JPanel wrapper = new JPanel(new GridLayout(1, renderCellArrays.size()));
            for(RenderCellArr cellArr : renderCellArrays) {
                wrapper.add(cellArr);
            }

            this.setLocationRelativeTo(null);
            this.setContentPane(wrapper);
            this.setVisible(true);
        }
    }


    public void setCell(Cell cell, Color color) {
        RenderCellArr cellArrX = this.renderCellArrays.get(cell.getPosX());
        cellArrX.setCellColor(cell.getPosY(), color);
        this.repaint();
    }
}
