package View.ConfigMenuComponents;

import javax.swing.*;

public class CreateButton extends JButton {
    public CreateButton() {
        this.setText("Create");
        this.setFocusPainted(true);
    }
}
