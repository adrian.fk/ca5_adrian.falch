package View.ConfigMenuComponents;

import javax.swing.*;
import java.awt.*;

public class DimensionsForm extends JPanel {
    private JLabel lHeight;
    private JLabel lWidth;

    private String[] cbHeightElements = {"4", "8", "12"};
    private String[] cbWidthElements = {"6", "12", "18"};

    private JComboBox<String> cbHeight;
    private JComboBox<String> cbWidth;

    public DimensionsForm() {
        this.setLayout(new GridBagLayout());

        this.lHeight = new JLabel("Flag's height:");
        this.lWidth = new JLabel("Flag's width:");

        this.cbHeight = new JComboBox<>(this.cbHeightElements);
        this.cbHeight.setSelectedIndex(0);

        this.cbWidth = new JComboBox<>(this.cbWidthElements);
        this.cbWidth.setSelectedIndex(0);

        GridBagConstraints c = new GridBagConstraints();

        configureLabelConstraints(c);
        this.add(lHeight, c);
        c.gridy = 1; //Swapping to second line
        this.add(lWidth, c);

        configureComboBoxConstraints(c);
        this.add(cbHeight, c);
        c.gridy = 1; //Swapping to second line
        this.add(cbWidth, c);
    }

    private void configureComboBoxConstraints(GridBagConstraints c) {
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 0;
        c.weighty = 1;
        c.weightx = 1;
        c.insets = new Insets(0, 25, 0, 0);
    }

    private void configureLabelConstraints(GridBagConstraints c) {
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        c.gridx = 0;

        c.gridy = 0;
        c.weighty = 0;
        c.weightx = 0;
    }

    public String getCbHeight() {
        return this.cbHeightElements[this.cbHeight.getSelectedIndex()];
    }

    public String getCbWidth() {
        return this.cbWidthElements[this.cbWidth.getSelectedIndex()];
    }
}
