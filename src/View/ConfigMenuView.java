package View;

import View.ConfigMenuComponents.CreateButton;
import View.ConfigMenuComponents.DimensionsForm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ConfigMenuView extends JFrame {
    private DimensionsForm form;
    private CreateButton button;

    public ConfigMenuView() throws HeadlessException {
        this.setTitle("AC5");
        this.form = new DimensionsForm();
        this.button = new CreateButton();

        this.form.setVisible(true);
        this.button.setVisible(true);

        JPanel content = new JPanel(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        int centerOffset = 30;
        configureFormConstraints(c, centerOffset);
        content.add(form, c);

        configureButtonConstraints(c, centerOffset);
        content.add(button, c);

        this.setSize(220, 230);
        this.setContentPane(content);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }

    public CreateButton getCreateButton() {
        return button;
    }

    public void attachListeners(ActionListener listener) {
        this.button.addActionListener(listener);
    }

    private void configureButtonConstraints(GridBagConstraints c, int centerOffset) {
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 0.5;
        c.weighty = 1;
        c.gridy = 2;
        c.gridx = 2;
        c.insets = new Insets(0, 100, 0, centerOffset);
        c.anchor = GridBagConstraints.LAST_LINE_END;
    }

    private void configureFormConstraints(GridBagConstraints c, int centerOffset) {
        c.gridwidth = 1;
        c.gridheight = 2;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0;
        c.gridx = 2;
        c.gridy = 1;
        c.insets = new Insets(0, centerOffset, 0, centerOffset);
        c.anchor = GridBagConstraints.PAGE_START;
    }

    public int getCbHeight() {
        String height = form.getCbHeight();
        return Integer.parseInt(height);
    }

    public int getCbWidth() {
        String width = form.getCbWidth();
        return Integer.parseInt(width);
    }

    public void displayErrorMsg() {
        JOptionPane.showMessageDialog(
                new JFrame(),
                "The height can not be grater nor equal to the width",
                "Warning",
                JOptionPane.WARNING_MESSAGE
        );
    }
}
